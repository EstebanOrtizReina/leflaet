class Mapa{

    miVisor;
    mapaBase;
    posicionInicial;
    escalaInicial;
    proveedorURL;
    atributosProveedor;
    marcadores=[]
    circulos=[];
    poligono=[];


    constructor(){
        this.posicionInicial=[4.589649,-74.125381]       
        this.escalaInicial=15
        this.proveedorURL='https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png'
        this.atributosProveedor={
            maxZoom:20
        }
        this.miVisor=L.map("mapid")
        this.miVisor.setView(this.posicionInicial,this.escalaInicial)
        this.mapaBase=L.tileLayer(this.proveedorURL,this.atributosProveedor)
        this.mapaBase.addTo(this.miVisor)

    }
    colocarMarcador(posicion){
        
        this.marcadores.push(L.marker(posicion))
        this.marcadores[0].addTo(this.miVisor)

    }
    colocarCirculo(posicion, configuracion){

        this.circulos.push(L.circle(posicion, configuracion))
        this.circulos[this.circulos.length-1].addTo(this.miVisor)


    }
    
    colocarPoligono(posicion) {

        this.poligono.push(L.polygon(posicion));
        this.poligono[this.poligono.length-1].addTo(this.miVisor);
    }
 




}
let miMapa=new Mapa();

miMapa.colocarMarcador([4.589649,-74.125381])

miMapa.colocarCirculo([4.622365, -74.18964], {
    color: 'red',
    fillColor: 'red',
    fillOpacity: 0.5,
    radius: 600
});
miMapa.colocarPoligono([
    [4.403689, -73.945189],
    [4.403916, -73.940380],
    [4.401000, -73.948753]   
])




